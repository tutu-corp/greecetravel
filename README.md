# Greeve Travel
Greeve Travel is an application to display page about Greece.

## General info
This project is based on an Udemy tutorial.

#### Project is created with :
* Swift 5
* IOS 16.4
* Xcode 14.0

## Screenshot
|Home|
|--|
|![Home](./captures/home.png)|

## Projects statut
This project is over.