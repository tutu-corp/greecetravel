//
//  GreeveTravelApp.swift of project named GreeveTravel
//
//  Created by Aurore D (@Tuwleep) on 02/05/2023.
//
//  

import SwiftUI

@main
struct GreeveTravelApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
