//
//  LittleImageView.swift of project named GreeveTravel
//
//  Created by Aurore D (@Tuwleep) on 02/05/2023.
//
//  

import SwiftUI

struct LittleImageView: View {
    
    var image: String
    
    var body: some View {
        Image(image)
            .resizable()
            .scaledToFill()
            .frame(width: 100, height: 100)
            .clipShape(RoundedRectangle(cornerRadius: 15))
    }
}

struct LittleImageView_Previews: PreviewProvider {
    static var previews: some View {
        LittleImageView(image: "bg")
    }
}
