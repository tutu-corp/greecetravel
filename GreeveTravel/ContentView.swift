//
//  ContentView.swift of project named GreeveTravel
//
//  Created by Aurore D (@Tuwleep) on 02/05/2023.
//
//  

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            
               ZStack {
                    Image("bg")
                        .resizable()
                        .scaledToFill()
                        .frame(alignment: .top)
                        .frame(height: 225)
                        .clipped()
                    HStack(alignment: .bottom){
                        Text("I 💙 Santorini")
                            .font(.largeTitle)
                            .fontWeight(.bold)
                            .foregroundColor(Color("GreeceBlue"))
                        Spacer()
                        Image("flag")
                            .resizable()
                            .frame(width: 80, height: 80.0)
                    }
                    .offset(y: 115)
                }
            Rectangle()
                .frame(height: 45)
                .foregroundColor(.clear)
            Divider()
                .frame(height: 2)
                .overlay(Color("GreeceBlue"))
            HStack{
                Text("Avis:")
                    .italic()
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star.fill")
                Image(systemName: "star")
            }
            .foregroundColor(Color("GreeceBlue"))
            ScrollView(.horizontal){
                HStack {
                    ForEach(1..<6) { index in
                        LittleImageView(image: "photo_\(index)")
                    }
                }
                
            }
            ScrollView(.vertical){
                Text("Santorin (en grec moderne : Σαντορίνη / Santoríni), aussi appelée Théra ou Thira (Θήρα / Thíra), est une île grecque située en mer Égée. C'est l'île la plus grande et la plus peuplée d'un petit archipel volcanique comprenant quatre autres îles, auquel on donne parfois son nom (archipel de Santorin).\nCette île et celles de Thirassía et Aspronissi sont les vestiges d'une ancienne île partiellement détruite vers 1610 av. J.-C. au cours de l'éruption minoenne.\nSantorin constitue l'un des principaux lieux touristiques de la Grèce, avec ses villages blancs à coupoles bleues perchés au sommet des falaises, ses panoramas sur les autres îles et ses sites archéologiques, notamment ceux de la ville antique de Théra et d'Akrotiri où furent retrouvées des ruines minoennes.")
                    .font(.title3)
                    .fontWeight(.regular)
                    .foregroundColor(Color("GreeceBlue"))
                
                    .padding()
            }
            .background(.gray)
            .cornerRadius(20)
            .padding(20)
            
            Spacer()
            Divider()
            HStack{
                Image(systemName: "square.and.arrow.up")
                Text("Je Partage")
            }
            .foregroundColor(Color("GreeceBlue"))
        }
        .ignoresSafeArea(edges: .top)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
